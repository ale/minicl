#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="minicl",
    version="0.2",
    url="https://git.autistici.org/ale/minicl.git",
    install_requires=[],
    setup_requires=[],
    zip_safe=True,
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "minicl-bootstrap = minicl.bootstrap:main",
            "minicl-config = minicl.config:main",
            "minicl-update = minicl.update:main",
            "minicl-update-users = minicl.users:main",
        ],
    },
)

