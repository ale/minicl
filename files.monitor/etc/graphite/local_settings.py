SECRET_KEY = 'sad%jk@)*(jan21._+H2'
ALLOWED_HOSTS = ['*']   # Django 1.5+

GRAPHITE_ROOT = '/usr/share/graphite-web'

CONF_DIR = '/etc/graphite'
STORAGE_DIR = '/var/lib/graphite/whisper'
CONTENT_DIR = '/usr/share/graphite-web/static'

WHISPER_DIR = '/var/lib/graphite/whisper'
DATA_DIRS = [WHISPER_DIR]
LOG_DIR = '/var/log/graphite'
INDEX_FILE = '/var/lib/graphite/search_index'

DATABASES = {
    'default': {
        'NAME': '/var/lib/graphite/graphite.db',
        'ENGINE': 'django.db.backends.sqlite3',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
