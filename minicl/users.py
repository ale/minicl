# Set up the required users and groups, and update their credentials
# (passwords and SSH keys).

import json
import optparse
import os

import minicl
from minicl import common


def homedir(username):
    return os.path.join('/home', username)


def gen_file(filename, usermap, line_gen):
    old_rows = []
    new_rows = []
    with open(filename, 'r') as fd:
        for line in fd:
            old_rows.append(line)
            try:
                username = line.split(':')[0]
                if username in usermap:
                    continue
            except:
                pass
            new_rows.append(line)

    for username, userinfo in sorted(usermap.iteritems()):
        new_rows.append(line_gen(username, userinfo) + '\n')

    if new_rows != old_rows:
        tmpf = minicl.ROOT + filename + '.tmp'
        with open(tmpf, 'w') as fd:
            fd.write(''.join(new_rows))
        common.copystat(filename, tmpf)
        os.rename(tmpf, minicl.ROOT + filename)


def gen_passwd(config, usermap):
    def _passwd(username, userinfo):
        return ':'.join((
            username,
            'x',
            str(userinfo['uid']),
            str(minicl.ADMIN_GROUP_ID),
            username,
            homedir(username),
            userinfo.get('shell', minicl.DEFAULT_SHELL)))

    return gen_file('/etc/passwd', usermap, _passwd)


def gen_shadow(usermap):
    def _shadow(username, userinfo):
        return ':'.join((
            username,
            userinfo['passwd'],
            '', '', '', '', '', '', ''))

    return gen_file('/etc/shadow', usermap, _shadow)


def gen_group():
    groupmap = {
        minicl.ADMIN_GROUP_NAME: minicl.ADMIN_GROUP_ID
    }
    return gen_file('/etc/group', groupmap,
                    lambda name, gid: '%s:x:%d:' % (name, gid))


def setup_homedir(config, username, userinfo):
    home = minicl.ROOT + homedir(username)
    if not os.path.isdir(home):
        os.mkdir(home, 0750)
        os.chown(home, userinfo['uid'], minicl.ADMIN_GROUP_ID)


def setup_ssh(config, username, userinfo):
    ssh_dir = minicl.ROOT + os.path.join(homedir(username), '.ssh')
    if not os.path.isdir(ssh_dir):
        os.mkdir(ssh_dir, 0700)
    else:
        os.chmod(ssh_dir, 0700)
    os.chown(ssh_dir, userinfo['uid'], minicl.ADMIN_GROUP_ID)

    keys = userinfo.get('ssh_key')
    if not keys:
        return
    if isinstance(keys, basestring):
        keys = [keys]
    with open(os.path.join(ssh_dir, 'authorized_keys'), 'w') as fd:
        for k in keys:
            fd.write('%s\n' % k)


def update_users(config):
    usermap = config['users']
    gen_passwd(config, usermap)
    gen_shadow(usermap)
    gen_group()
    for username, userinfo in usermap.iteritems():
        setup_homedir(config, username, userinfo)
        setup_ssh(config, username, userinfo)


def _main():
    parser = optparse.OptionParser()
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')

    update_users(common.read_config())

    return 0


def main():
    common.run(_main)
