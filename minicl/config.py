# Query the cluster config and output the desired result in a format
# that is usable by the shell.

import json
import optparse
import os

import minicl
from minicl import common


def _main():
    parser = optparse.OptionParser()
    opts, args = parser.parse_args()

    if not args:
        parser.error('Must specify a query')
    if len(args) > 1:
        parser.error('Too many arguments')

    # Run the query.
    query = args[0]
    config = common.read_config()
    for key in query.split('.'):
        if key in config:
            config = config[key]
        else:
            return 1

    # Show the results.
    if isinstance(config, dict):
        for key in config.iterkeys():
            print key
    elif isinstance(config, list):
        for item in config:
            print item
    else:
        print config

    return 0


def main():
    common.run(_main)
