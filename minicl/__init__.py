import os

# This file contains global settings, some of which can be overriden
# from the environment (mainly for testing purposes).

# Location of the cluster configuration file.
CLUSTER_CONFIG = os.getenv('CLUSTER_CONFIG', '/etc/cluster.json')

# Location of the deploy key file.
DEPLOY_KEY = os.getenv('DEPLOY_KEY', '/root/.ssh/deploy')

# Root directory for all output.
ROOT = os.getenv('DESTDIR', '')

# Name and ID of the administrative group.
ADMIN_GROUP_NAME = 'admins'
ADMIN_GROUP_ID = 755

# Location of the local repository.
REPO_DIR = '/var/lib/minicl/repo'

# Default shell for users.
DEFAULT_SHELL = '/bin/bash'

