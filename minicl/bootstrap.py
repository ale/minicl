# Bootstrap a system, given the config repository URL.

import json
import optparse
import os
import subprocess

import minicl
from minicl import common
from minicl.update import copy_deploy_key, create_root_ssh_config, \
    update_repo, update_system


BOOTSTRAP_PACKAGES = ['git', 'openssh-client', 'slack']


def get_ips():
    return subprocess.check_output(['hostname', '-I']).split()


def install_packages():
    subprocess.check_call(
        ['apt-get', '-qq', '-y', 'install'] + BOOTSTRAP_PACKAGES,
        env={'DEBIAN_FRONTEND': 'noninteractive',
             'PATH': os.getenv('PATH')})


def check_hostname():
    # Make sure everything is set properly and it will work on reboot.
    short_hostname = subprocess.check_output(['hostname', '-s']).strip()
    if '.' in short_hostname:
        short_hostname = short_hostname.split('.')[0]
    with open('/etc/hostname', 'w') as fd:
        fd.write('%s\n' % short_hostname)
    subprocess.check_call(['hostname', short_hostname])
    return short_hostname


def create_root_ssh_key(key_type):
    ssh_dir = minicl.ROOT + '/root/.ssh'
    if not os.path.exists(ssh_dir):
        os.mkdir(ssh_dir, 0700)
    key_file = '%s/root/.ssh/id_%s' % (minicl.ROOT, key_type)
    if not os.path.exists(key_file):
        subprocess.check_call(
            ['ssh-keygen', '-N', '', '-t', key_type, '-f', key_file])
    pub_file = key_file + '.pub'
    with open(pub_file, 'r') as fd:
        return fd.read().strip()


def _main():
    parser = optparse.OptionParser()
    parser.add_option('--repo', metavar='URL')
    parser.add_option('--ssh-key-type', metavar='TYPE', dest='ssh_key_type',
                      default='ed25519', help='SSH key type for root')
    parser.add_option('--deploy-key', metavar='FILE', dest='deploy_key')
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')
    if not opts.repo:
        parser.error('You must specify the URL for your repository '
                     'with the --repo option')

    # If the user has passed a deploy key, set up root's SSH config
    # to use it. Otherwise we assume (and check) that a SSH agent is
    # running.
    if common.is_ssh_url(opts.repo):
        if opts.deploy_key:
            copy_deploy_key(opts.deploy_key)
            create_root_ssh_config(opts.repo)
        elif not os.getenv('SSH_AUTH_SOCK'):
            raise Exception('SSH agent was not detected.')

    # Install required packages.
    install_packages()

    # Check that the hostname is set properly.
    hostname = check_hostname()

    # Create a SSH key for the root user.
    root_ssh_key = create_root_ssh_key(opts.ssh_key_type)
    
    # Update the repository the first time, read the configuration
    # from it and use it to configure the local system.
    update_repo(opts.repo)
    config = common.read_config(os.path.join(
        minicl.REPO_DIR, 'cluster.json'))
    update_system(config)

    # Run 'slack base' to bootstrap the rest of the system.
    subprocess.check_call(['slack', '-v', 'base'])

    # Print out the configuration snippet for this host.
    host_config = {
        'ip': get_ips(),
        'roles': ['base'],
        'root_ssh_key': root_ssh_key,
    }
    print '\n\n\nBootstrap complete.'
    print 'Add the following snippet to your cluster.json:\n'
    print json.dumps({hostname: host_config}, indent=4)

    return 0


def main():
    common.run(_main)
