import json
import os
import re
import shutil
import sys

import minicl


def read_config(path=None):
    if not path:
        path = minicl.CLUSTER_CONFIG
    with open(path, 'r') as fd:
        return json.load(fd)


def is_ssh_url(s):
    """Return True if 's' looks like a SSH URL for a git repository."""
    if re.match(r'^[^/]+@.*:.*', s):
        return True
    return False


def copyfileattrs(src, dst, mode, uid, gid):
    """Copy a file and set its owner/mode."""
    shutil.copy2(src, dst)
    os.chmod(dst, mode)
    os.chown(dst, uid, gid)


def copystat(src, dst):
    """Copy file owner/mode from src to dst."""
    src_stat = os.stat(src)
    os.chown(dst, src_stat.st_uid, src_stat.st_gid)
    os.chmod(dst, src_stat.st_mode)


def run(f):
    """Run 'f', catch exceptions and return appropriate exit status."""
    try:
        exit_status = f()
    except Exception as e:
        print >>sys.stderr, 'ERROR: %s' % e
        exit_status = 2
    sys.exit(exit_status)
