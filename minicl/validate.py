# Pre-update hook for local git repositories that validates the
# cluster.json file contents before pushing changes upstream.

import re
import sys
from minicl import common


def _validate_user(username, userinfo):
    required_attrs = [
        'uid',
        'passwd',
        'ssh_key',
    ]
    for attr in required_attrs:
        if attr not in userinfo:
            raise Exception('user %s: missing required attribute %s' % (
                username, attr))

    ssh_keys = userinfo.get('ssh_key', [])
    if isinstance(ssh_keys, basestring):
        ssh_keys = [ssh_keys]
    for ssh_key in ssh_keys:
        if not re.match(r'^ssh-\w+ [A-Za-z0-9+/]+=* .*$', ssh_key):
            raise Exception(
                'user %s: ssh_key "%s..." does not look like a SSH public key' % (
                    username, ssh_key[:32]))


def _validate_host(hostname, hostinfo):
    required_attrs = [
        'ip',
    ]
    for attr in required_attrs:
        if attr not in hostinfo:
            raise Exception('host %s: missing required attribute %s' % (
                hostname, attr))
    ips = hostinfo['ip']
    if not isinstance(ips, list) and not isinstance(ips, basestring):
        raise Exception('host %s: "ip" should be a list or a string' % (
            hostname))


def _validate_smtp(smtp):
    required_attrs = [
        'relay',
        'username',
        'password',
    ]
    for attr in required_attrs:
        if attr not in smtp:
            raise Exception('smtp: missing required attribute %s' % attr)


def validate(config):
    required_attrs = [
        'domain',
        'admin_email',
        'users',
        'hosts',
        'smtp',
    ]
    for attr in required_attrs:
        if attr not in config:
            raise Exception('config missing required attribute %s' % attr)
    for username, userinfo in config['users'].iteritems():
        _validate_user(username, userinfo)
    for hostname, hostinfo in config['hosts'].iteritems():
        _validate_host(hostname, hostinfo)
    _validate_smtp(config['smtp'])


def _main():
    if len(sys.argv) != 2:
        print >>sys.stderr, 'Usage: validate cluster.json'
        return 1

    arg = sys.argv[1]
    config = common.read_config(arg)
    validate(config)
    return 0


def main():
    common.run(_main)


if __name__ == '__main__':
    main()
