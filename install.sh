#!/bin/sh
#
# Quick installation script for new hosts.
#

export DEBIAN_FRONTEND=noninteractive LANG=C

if [ `whoami` != root ]; then
  echo "This script should be run as root!" >&2
  exit 1
fi

set -e

# We need wget.
[ -x /usr/bin/wget ] || apt-get -q -y install wget

# Set up the package sources.
REPO_KEY=CE9ED9B0
if ! apt-key list | grep -q ${REPO_KEY}; then
    wget -q -O- https://www.incal.net/debian/repo.key | apt-key add -
fi
echo 'deb http://www.incal.net/debian/common ./' \
    > /etc/apt/sources.list.d/common.list

apt-get update
apt-get install -q -y minicl git ca-certificates wget

# Install the A/I CA certificate (for git.autistici.org) as a convenience.
ca_url=http://www.autistici.org/static/certs/ca.crt
if [ ! -e /usr/local/share/ca-certificates/autistici.org.crt ]; then
  wget -q -O /usr/local/share/ca-certificates/autistici.org.crt $ca_url
  update-ca-certificates
fi

exit 0
